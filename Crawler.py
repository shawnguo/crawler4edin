#!/usr/bin/python
# -*- coding: utf-8 -*-
# @Time    : 2018/11/21 21:17
# @Author  : Shawn
# @File    : Crawler.py
# @Classes : Crawler

import requests
from bs4 import BeautifulSoup as bs
import urllib3
from selenium import webdriver
import os


page_url = "https://www.inf.ed.ac.uk/teaching/courses/mlpr/2018/lectures/"
out_path = "E:\\Edin\\Courses_Resources\\MLPR\\Lectures\\"

# the following block is particular for click event
# comment if you don't need
# chromedriver = "./driver/chromedriver"
# os.environ["webdriver.chrome.driver"] = chromedriver
# driver = webdriver.Chrome(chromedriver)
# driver.get(page_url)
# driver.find_element_by_link_text("toggle the pdf links").click()


r = requests.get(page_url)
soup = bs(r.text,  'html.parser')
urls = []
names = []
for i, link in enumerate(soup.findAll('a')):
    if link.get('href') is not None:
        _FULLURL = page_url + link.get('href')
        if _FULLURL.endswith('.pdf'):
            urls.append(_FULLURL)
            names.append(soup.select('a')[i].attrs['href'])

names_urls = zip(names, urls)

print(names)
print(urls)

for name, url in names_urls:
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    f = open(out_path + name, 'wb')
    f.write(response.data)
    f.close()
